<?php

namespace App\Http\Controllers;

use App\Http\Requests\Carmodel\StoreRequest;
use App\Http\Requests\Carmodel\UpdateRequest;
use App\Http\Resources\Carmodel\CarmodelResource;
use App\Models\Carmodel;
use Illuminate\Support\Facades\Log;


class CarmodelController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $carmodels = Carmodel::all();

        return CarmodelResource::collection($carmodels);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreRequest $request)
    {
        $data = $request->validated();
        $carmodel = Carmodel::create($data);

        return CarmodelResource::make($carmodel);
    }

    /**
     * Display the specified resource.
     */
    public function show(Carmodel $carmodel)
    {
        return CarmodelResource::make($carmodel);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Brand $brand)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRequest $request, Carmodel $carmodel)
    {
        $data = $request->validated();
        $carmodel->update($data);

        $carmodel = $carmodel->fresh();

        return CarmodelResource::make($carmodel);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Carmodel $carmodel)
    {
        $carmodel->delete();

        return response()->json([
            'message' => 'done',
        ]);
    }

}
