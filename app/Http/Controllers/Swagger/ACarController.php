<?php

namespace App\Http\Controllers\Swagger;

use App\Http\Controllers\Controller;

/**
 *
 * @OA\Post(
 *     path="/api/cars",
 *     summary="Создание автомобиля",
 *     tags={"Автомобили"},
 *     security={{ "bearerAuth": {} }},
 *
 *     @OA\RequestBody(
 *         @OA\JsonContent(
 *             allOf={
 *                 @OA\Schema(
 *                     @OA\Property(property="year", type="integer", example=2020),
 *                     @OA\Property(property="mileage", type="integer", example=93585),
 *                     @OA\Property(property="color", type="string", example="Some color of a car"),
 *                     @OA\Property(property="brand_id", type="integer", example=1),
 *                     @OA\Property(property="carmodel_id", type="integer", example=1),
 *                     @OA\Property(property="user_id", type="integer", example=1),
 *                 )
 *             }
 *         )
 *     ),
 *
 *     @OA\Response(
 *         response=200,
 *         description="Ok",
 *         @OA\JsonContent(
 *             @OA\Property(property="data", type="object",
 *                 @OA\Property(property="id", type="integer", example=1),
 *                 @OA\Property(property="year", type="integer", example=2020),
 *                 @OA\Property(property="mileage", type="integer", example=93585),
 *                 @OA\Property(property="color", type="string", example="Some color of a car"),
 *                 @OA\Property(property="brand_id", type="integer", example=1),
 *                 @OA\Property(property="carmodel_id", type="integer", example=1),
 *                 @OA\Property(property="user_id", type="integer", example=1),
 *             ),
 *         ),
 *     ),
 * ),
 *
 * @OA\Get(
 *     path="/api/cars",
 *     summary="Список автомобилей",
 *     tags={"Автомобили"},
 *     security={{ "bearerAuth": {} }},
 *
 *     @OA\Response(
 *         response=200,
 *         description="Ok",
 *         @OA\JsonContent(
 *             @OA\Property(property="data", type="array", @OA\Items(
 *                 @OA\Property(property="id", type="integer", example=1),
 *                 @OA\Property(property="year", type="integer", example=2020),
 *                 @OA\Property(property="mileage", type="integer", example=93585),
 *                 @OA\Property(property="color", type="string", example="Some color of a car"),
 *                 @OA\Property(property="brand_id", type="integer", example=1),
 *                 @OA\Property(property="carmodel_id", type="integer", example=1),
 *                 @OA\Property(property="user_id", type="integer", example=1),
 *             )),
 *         ),
 *     ),
 * ),
 *
 * @OA\Get(
 *     path="/api/cars/{car}",
 *     summary="Получить один автомобиль",
 *     tags={"Автомобили"},
 *     security={{ "bearerAuth": {} }},
 *
 *     @OA\Parameter(
 *         description="ID",
 *         in="path",
 *         name="car",
 *         required=true,
 *         example=1,
 *     ),
 *
 *     @OA\Response(
 *         response=200,
 *         description="Ok",
 *         @OA\JsonContent(
 *             @OA\Property(property="data", type="object",
 *                 @OA\Property(property="id", type="integer", example=1),
 *                 @OA\Property(property="year", type="integer", example=2020),
 *                 @OA\Property(property="mileage", type="integer", example=93585),
 *                 @OA\Property(property="color", type="string", example="Some color of a car"),
 *                 @OA\Property(property="brand_id", type="integer", example=1),
 *                 @OA\Property(property="carmodel_id", type="integer", example=1),
 *                 @OA\Property(property="user_id", type="integer", example=1),
 *             ),
 *         ),
 *     ),
 * ),
 *
 * @OA\Patch(
 *     path="/api/cars/{car}",
 *     summary="Обновить автомобиль",
 *     tags={"Автомобили"},
 *     security={{ "bearerAuth": {} }},
 *
 *     @OA\Parameter(
 *         description="ID",
 *         in="path",
 *         name="car",
 *         required=true,
 *         example=2,
 *     ),
 *
 *     @OA\RequestBody(
 *         @OA\JsonContent(
 *             allOf={
 *                 @OA\Schema(
 *                     @OA\Property(property="year", type="integer", example=2020),
 *                     @OA\Property(property="mileage", type="integer", example=93585),
 *                     @OA\Property(property="color", type="string", example="Some color of a car"),
 *                     @OA\Property(property="brand_id", type="integer", example=1),
 *                     @OA\Property(property="carmodel_id", type="integer", example=1),
 *                     @OA\Property(property="user_id", type="integer", example=1),
 *                 )
 *             }
 *         )
 *     ),
 *
 *     @OA\Response(
 *         response=200,
 *         description="Ok",
 *         @OA\JsonContent(
 *             @OA\Property(property="data", type="object",
 *             @OA\Property(property="id", type="integer", example=1),
 *             @OA\Property(property="year", type="integer", example=2020),
 *             @OA\Property(property="mileage", type="integer", example=93585),
 *             @OA\Property(property="color", type="string", example="Some color of a car"),
 *             @OA\Property(property="brand_id", type="integer", example=1),
 *             @OA\Property(property="carmodel_id", type="integer", example=1),
 *             @OA\Property(property="user_id", type="integer", example=1),
 *             ),
 *         ),
 *     ),
 * ),
 *
 * @OA\Delete(
 *     path="/api/cars/{car}",
 *     summary="Удалить автомобиль",
 *     tags={"Автомобили"},
 *     security={{ "bearerAuth": {} }},
 *
 *     @OA\Parameter(
 *         description="ID",
 *         in="path",
 *         name="car",
 *         required=true,
 *         example=1,
 *     ),
 *
 *     @OA\Response(
 *         response=200,
 *         description="Ok",
 *         @OA\JsonContent(
 *             @OA\Property(property="message", type="string", example="done"),
 *         ),
 *     ),
 * ),
 */
class ACarController extends Controller
{

}
