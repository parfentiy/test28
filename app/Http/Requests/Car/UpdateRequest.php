<?php

namespace App\Http\Requests\Car;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'year' => 'integer',
            'mileage' => 'integer',
            'color' => 'string',
            'brand_id' => 'required|integer',
            'carmodel_id' => 'required|integer',
            'user_id' => 'required|integer',
        ];
    }
}
