<?php

namespace App\Http\Resources\Carmodel;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class CarmodelResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'brand_id' => $this->brand_id,
        ];
    }
}
