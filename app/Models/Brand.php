<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    use HasFactory;

    protected $guarded = false;

    protected $fillable = [
        'title',
    ];

    public function carmodels()
    {
        return $this->hasMany(Carmodel::class);
    }

    public function cars()
    {
        return $this->hasMany(Car::class);
    }


}
