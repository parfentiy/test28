<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    use HasFactory;

    protected $guarded = false;

    protected $fillable = [
        'year',
        'mileage',
        'color',
        'brand_id',
        'carmodel_id',
        'user_id',
    ];

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function carmodel()
    {
        return $this->belongsTo(Carmodel::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

}
