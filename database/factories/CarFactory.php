<?php

namespace Database\Factories;

use App\Models\Brand;
use App\Models\Carmodel;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Car>
 */
class CarFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $randomBrand = fake()->numberBetween(1, Brand::all()->count());
        return [
            'year' => fake()->numberBetween(1980, 2023),
            'mileage' => fake()->numberBetween(5000, 200000),
            'color' => fake()->randomElement(['blue', 'red', 'green', 'black', 'white', 'yellow', 'brown']),
            'brand_id' => $randomBrand,
            'carmodel_id' => fake()->randomElement(Carmodel::where('brand_id', $randomBrand)->pluck('id')),
            'user_id' => fake()->randomElement(User::all()->pluck('id')),
        ];
    }
}
