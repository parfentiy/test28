<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('cars', function (Blueprint $table) {
            $table->id();

            $table->integer('year')->nullable();
            $table->integer('mileage')->nullable();
            $table->string('color')->nullable();
            $table->timestamps();

            $table->unsignedBigInteger('brand_id')->nullable();
            $table->index('brand_id', 'car_brand_idx');
            $table->foreign('brand_id', 'car_brand_fk')
                ->on('brands')->references('id');

            $table->unsignedBigInteger('carmodel_id')->nullable();
            $table->index('carmodel_id', 'car_carmodel_idx');
            $table->foreign('carmodel_id', 'car_carmodel_fk')
                ->on('carmodels')->references('id');

            $table->unsignedBigInteger('user_id')->nullable();
            $table->index('user_id', 'car_user_idx');
            $table->foreign('user_id', 'car_user_fk')
                ->on('users')->references('id');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('cars');
    }
};
