<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Car;
use App\Models\Carmodel;
use App\Models\Brand;
use App\Models\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $brands = [
            'Volvo' => ['XC90', 'S60', 'C30'],
            'Skoda' => ['Rapid', 'Fabia', 'SuperB'],
            'Toyota' => ['Camry', 'LC200', 'Corolla'],
        ];

        Car::truncate();
        Carmodel::truncate();
        Brand::truncate();
        User::truncate();

        for($i=1; $i<=3; $i++) {
            User::factory()->create([
                'name' => 'user' . $i,
                'email' => 'test'. $i. '@mail.com',
                'password' => '123123123',
            ]);
        }

        foreach ($brands as $brand => $carModels) {
            Brand::factory()->create([
                'title' => $brand,
            ]);

            foreach ($carModels as $carModel) {
                Carmodel::factory()->create([
                    'title' => $carModel,
                    'brand_id' => Brand::whereTitle($brand)->first()->id,
                ]);
            }
        }

        \App\Models\Car::factory(15)->create();
    }
}
