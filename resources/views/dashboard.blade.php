<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="/bootstrap.min.css" rel="stylesheet">
    <script src="/bootstrap.bundle.min.js"></script>
    <style>
        form {
            margin: 1; /* Убираем отступы */
        }
    </style>
</head>

<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 text-gray-900">
                    {{ __("You're logged in!") }}
                </div>
            </div>
        </div>
    </div>
    <div class="py-12 max-w-7xl mx-auto sm:px-6 lg:px-8 bg-white overflow-hidden shadow-sm sm:rounded-lg">
        <div class="fw-bold">Ваши автомобили</div>

        <table class="table table-sm w-auto table-bordered table-striped table-hover caption-top align-top">
            <thead class="table-light text-center align-top">
            <tr>
                <th scope="col">ID</th>
                <th scope="col">Марка</th>
                <th scope="col">Модель</th>
                <th scope="col">Год выпуска</th>
                <th scope="col">Цвет</th>
                <th scope="col">Пробег</th>
            </tr>
            </thead>
            <tbody class="text-center">
            @foreach(\App\Models\Car::where('user_id', Auth::user()->id)->get() as $car)
                <tr>
                    <td>
                        {{$car->id}}
                    </td>
                    <td>
                        {{$car->brand->title}}
                    </td>
                    <td>
                        {{$car->carmodel->title}}
                    </td>
                    <td>
                        {{$car->year}}
                    </td>
                    <td>
                        {{$car->color}}
                    </td>
                    <td>
                        {{$car->mileage}} км.
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</x-app-layout>
